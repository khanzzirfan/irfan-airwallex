import React from 'react';
import * as Api from './index';
import { shallow, mount, render } from 'enzyme';

describe("testing fetch api service", () => {

    const message = 'message';
    const mockResponse = (status, statusText, response) => {
        return new Response(response, {
            status: status,
            statusText: statusText,
            headers: {
                'Content-type': 'application/json'
            }
        });
    };

    var originalTimeout;
    let actualResult;
    let fetchUrl = 'https://l94wc2001h.execute-api.ap-southeast-2.amazonaws.com/prod/fake-auth';
    let stubResponse = "Successful";

    beforeEach(function () {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 100;
    });

    afterEach(function () {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    });

    it('fetch is called with the correct params', async (done) => {
        const fakePromise = Promise.resolve(mockResponse(
            200,
            null,
            JSON.stringify({ message: message })
        ));

        window.fetch = jest.fn().mockImplementationOnce(() => {
            return fakePromise
        });

        Api.submitRequest('foo', 'ik@gmail.com').then(r => {
            //console.log(window.fetch);
            expect(window.fetch).toHaveBeenCalled();
            expect(window.fetch.mock.calls[0][0]).toEqual(fetchUrl);
            expect(window.fetch.mock.calls[0][1]).toEqual({
                body: JSON.stringify({ name: 'foo', email: 'ik@gmail.com' }),
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                },
                method: 'POST',
            });
            done();

        }).catch(err => {
            done();
        });
    });


    it('error is returned when bad request sumbitted', async (done) => {
        window.fetch = jest.fn().mockImplementationOnce(() => new Promise((resolve, reject) => {
            reject(new Error('failed'))
        }))

        Api.submitRequest('foo', 'ik@gmail.com').then(r => {
            console.log(window.fetch);
            expect(window.fetch).toHaveBeenCalled();
            expect(window.fetch.mock.calls[0][0]).toEqual(fetchUrl);
            done();

        }).catch(err => {
            expect(err.message).toEqual('failed');
            done();
        });
    });

});