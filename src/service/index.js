
const submitRequest = async (fullName, email) => {

    const fetchUrl = 'https://l94wc2001h.execute-api.ap-southeast-2.amazonaws.com/prod/fake-auth';
    const requestBody = JSON.stringify({
        name: fullName,
        email: email,
    });

    const result = await fetch(fetchUrl, {
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        },
        method: 'POST',
        body: requestBody,
    });

    const response = await parseJSON(result);
    if (response.ok) {
        return response.json;
    } else {
        throw response.json.errorMessage;
    }
}

/**
 * Parses the JSON returned by a network request
 *
 * @param  {object} response A response from a network request
 *
 * @return {object}          The parsed JSON, status from the response
 */
function parseJSON(response) {
    return new Promise((resolve) => response.json()
        .then((json) => resolve({
            status: response.status,
            ok: response.ok,
            json,
        })));
}


export { submitRequest };