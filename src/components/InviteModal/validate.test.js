//import 'jsdom-global/register';
import React from 'react';
import { shallow, mount, render } from 'enzyme';
import validate from './validate';

describe("validate state", () => {

    it("should show required for fullname, email, confirmation email", () => {
        let errors = validate({});
        expect(errors).toEqual({
            'ConfirmEmail': 'Required',
            'Email': 'Required',
            'FullName': 'Required'
        });
    });

    it("should show error if full name is less than 3 characters", () => {
        let errors = validate({ FullName: 'ab' });
        expect(errors).toEqual({
            'ConfirmEmail': 'Required',
            'Email': 'Required',
            'FullName': 'Name must be atleast 3 characters long'
        });

        //no full name error
        errors = validate({ FullName: 'abc' });
        expect(errors).toEqual({
            'ConfirmEmail': 'Required',
            'Email': 'Required',
        });
    });


    it("should show error if not a valid email", () => {

        let invalidEmailAddress = ['plainaddress',
            '#@%^%#$@#$@#.com',
            '@domain.com',
            'Joe Smith <email@domain.com>',
            'email.domain.com',
            'email@domain@domain.com',
            'email..email@domain.com	',
            'あいうえお@domain.com',
            'email@domain.com (Joe Smith)',
            'email@domain',
            'email@111.222.333.44444'
        ];

        invalidEmailAddress.map(item => {
            let errors = validate({ FullName: 'abcd', Email: item });
            expect(errors).toEqual({
                'ConfirmEmail': 'Required',
                'Email': 'Invalid email address',
            });
        })
    });

    it("should not show error for a valid email", () => {

        let validEmailAddress = [
            'email@domain.com',
            'firstname.lastname@domain.com',
            'email@subdomain.domain.com',
            'firstname+lastname@domain.com',
            '1234567890@domain.com',
            'email@domain-one.com',
            'email@domain.name',
            'email@domain.co.jp',
            'firstname-lastname@domain.com',
        ];

        validEmailAddress.map(item => {
            let errors = validate({ FullName: 'abcd', Email: item });
            expect(errors).toEqual({
                'ConfirmEmail': 'Required',
            });
        })
    });

    it("should show error if email and confirm email does not match", ()=> {

        let errors = validate({ FullName: 'abc', Email:'ik@gmail.com', ConfirmEmail:'ikbc@gmailcom' });
        expect(errors).toEqual({
            'ConfirmEmail': 'Email confirmation does not match',
        });

    });

    it("should show not show any error", ()=> {
        let errors = validate({ FullName: 'abcd', Email:'ik@gmail.com', ConfirmEmail:'ik@gmail.com' });
        expect(errors).toEqual({});

    });
});