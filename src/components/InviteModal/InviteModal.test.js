//import 'jsdom-global/register';
import React from 'react';
import InviteModal from './InviteModal';
import { shallow, mount, render } from 'enzyme';

describe("InviteModal on default state", () => {

    let componentWrapper = null;
    let stubProps = {
        show: false,
        isRequestInProgress: false,
        registerService: jest.fn(),
        onCancel: jest.fn(),
        registrationError: '',
    };

    beforeEach(() => {
        componentWrapper = shallow(<InviteModal {...stubProps}/>);
    });

    it("component should exists and maps snapshot", ()=>{
        expect(componentWrapper).toBeDefined();
        expect(componentWrapper).toMatchSnapshot();
    });

    it("should render the component with given props", ()=>{
        expect(componentWrapper.instance().props.show).toEqual(false);
        expect(componentWrapper.instance().props.isRequestInProgress).toEqual(false);
        expect(componentWrapper.instance().props.registrationError).toEqual('');
    });
});


describe("InviteModal -> show modal state", () => {

    let componentWrapper = null;
    let stubProps = {
        show: true,
        isRequestInProgress: false,
        registerService: jest.fn(),
        onCancel: jest.fn(),
        registrationError: '',
    };

    beforeEach(() => {
        componentWrapper = shallow(<InviteModal {...stubProps}/>);
    });

    it("component should exists and maps snapshot", ()=>{
        expect(componentWrapper).toBeDefined();
        expect(componentWrapper).toMatchSnapshot();
        
    });

    it("should render the component with given props", ()=>{
        expect(componentWrapper.instance().props.show).toEqual(true);
        expect(componentWrapper.instance().props.isRequestInProgress).toEqual(false);
        expect(componentWrapper.instance().props.registrationError).toEqual('');
    });

    it("should show required field error when no data entered", ()=> {

        let wrapper = mount(<InviteModal {...stubProps}/>);
        //simulate click;
        wrapper.find({id:'sendrequest'}).first().simulate('click');

        //assert
        expect(wrapper.state().error).toEqual({
            'ConfirmEmail': 'Required',
            'Email':'Required',
            'FullName':'Required'
        });

        let element = wrapper.find({id:'FullName-Error'}).first();
        expect(element.text()).toEqual('Required');

        element = wrapper.find({id:'Email-Error'}).first();
        expect(element.text()).toEqual('Required');

        element = wrapper.find({id:'ConfirmEmail-Error'}).first();
        expect(element.text()).toEqual('Required');

    });

    it("should show required atleast 3 characters for name", ()=> {

        let wrapper = mount(<InviteModal {...stubProps}/>);
        wrapper.setState({ FullName: 'ba' });
        //simulate click;
        wrapper.find({id:'sendrequest'}).first().simulate('click');

        //assert
        expect(wrapper.state().error).toEqual({
            'ConfirmEmail': 'Required',
            'Email':'Required',
            'FullName':'Name must be atleast 3 characters long'
        });

        let element = wrapper.find({id:'FullName-Error'}).first();
        expect(element.text()).toEqual('Name must be atleast 3 characters long');
    });


    it("should show required is not a valid email address", ()=> {

        let wrapper = mount(<InviteModal {...stubProps}/>);
        wrapper.setState({ FullName: 'bacd', Email:'ikd.com' });
        //simulate click;
        wrapper.find({id:'sendrequest'}).first().simulate('click');

        //assert
        expect(wrapper.state().error).toEqual({
            'ConfirmEmail': 'Required',
            'Email':'Invalid email address',
        });

        let element = wrapper.find({id:'Email-Error'}).first();
        expect(element.text()).toEqual('Invalid email address');
    });

    it("should show required is matching email address", ()=> {

        let wrapper = mount(<InviteModal {...stubProps}/>);
        wrapper.setState({ FullName: 'bacd', Email:'ik@gmail.com', ConfirmEmail:'ik@bmain.com' });
        //simulate click;
        wrapper.find({id:'sendrequest'}).first().simulate('click');

        //assert
        expect(wrapper.state().error).toEqual({
            'ConfirmEmail': 'Email confirmation does not match',
        });

        let element = wrapper.find({id:'ConfirmEmail-Error'}).first();
        expect(element.text()).toEqual('Email confirmation does not match');
    });

    it("should show no errors", ()=> {

        let wrapper = mount(<InviteModal {...stubProps}/>);
        wrapper.setState({ FullName: 'bacd', Email:'ik@gmail.com', ConfirmEmail:'ik@gmail.com' });
        //simulate click;
        wrapper.find({id:'sendrequest'}).first().simulate('click');
        //assert
        expect(wrapper.state().error).toEqual({});
    });

    it("should show spinner", ()=> {

        let wrapper = mount(<InviteModal {...stubProps} isRequestInProgress={true} />);
        wrapper.setState({ FullName: 'bacd', Email:'ik@gmail.com', ConfirmEmail:'ik@gmail.com' });
        //simulate click;
        let spinnerElement = wrapper.find({id:'spinner'});
        //assert
        expect(spinnerElement.length).toEqual(1);


        //hide spinner;
        wrapper = mount(<InviteModal {...stubProps} isRequestInProgress={false} />);
        wrapper.setState({ FullName: 'bacd', Email:'ik@gmail.com', ConfirmEmail:'ik@gmail.com' });
        //simulate click;
        spinnerElement = wrapper.find({id:'spinner'});
        //assert
        expect(spinnerElement.length).toEqual(0);
    });

    it("should show request status text when request in progress", ()=> {

        let wrapper = mount(<InviteModal {...stubProps} isRequestInProgress={true} />);
        wrapper.setState({ FullName: 'bacd', Email:'ik@gmail.com', ConfirmEmail:'ik@gmail.com' });
        //simulate click;
        let element = wrapper.find({id:'requestStatusText'});
        //assert
        expect(element.length).toEqual(1);
        expect(element.text()).toEqual("Sending... Please wait.");

        //hide spinner;
        wrapper = mount(<InviteModal {...stubProps} isRequestInProgress={false} />);
        wrapper.setState({ FullName: 'bacd', Email:'ik@gmail.com', ConfirmEmail:'ik@gmail.com' });
        //simulate click;
        element = wrapper.find({id:'requestStatusText'});
        //assert
        expect(element.text()).toEqual("");
    });

    it("should call on cancel onClick button", ()=> {
        const onCancelClick = jest.fn();
        let wrapper = mount(<InviteModal {...stubProps} isRequestInProgress={true} onCancel={onCancelClick}/>);
        //simulate click;
        wrapper.find({id:'cancelrequest'}).first().simulate('click');
        //assert
        expect(onCancelClick).toHaveBeenCalled();
    });

    it("should call on submit onClick button and return fullname, email", ()=> {
        const onClick = jest.fn();
        let wrapper = mount(<InviteModal {...stubProps} isRequestInProgress={false} registerService={onClick}/>);
        wrapper.setState({ FullName: 'irfan', Email:'ik@gmail.com', ConfirmEmail:'ik@gmail.com' });
        //simulate click;
        wrapper.find({id:'sendrequest'}).first().simulate('click');
        //assert
        expect(onClick).toHaveBeenCalled();
        expect(onClick).toHaveBeenCalledWith("irfan", "ik@gmail.com");
    });

    
});