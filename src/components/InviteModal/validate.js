const validate = (values, force) => {
    const errors = {};
    if (!values.FullName) {
        errors.FullName = 'Required';
    }else if(values.FullName.length < 3){
        errors.FullName = 'Name must be atleast 3 characters long';
    }
    if(values.Email==='' && !(force)){
        //for empty value do nothing, when not forced
    } else if (!values.Email) {
        errors.Email = 'Required';
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.Email)) {
        errors.Email = 'Invalid email address';
    }
    if(values.ConfirmEmail==='' && !(force)){
        //for empty value do nothing, when not forced
    } else if (!values.ConfirmEmail) {
        errors.ConfirmEmail = 'Required';
    } else if(values.Email && values.ConfirmEmail && (values.Email !== values.ConfirmEmail)){
        errors.ConfirmEmail = "Email confirmation does not match";
    }
    return errors;
};

export default validate;