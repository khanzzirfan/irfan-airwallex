import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Input, FormFeedback } from 'reactstrap';
import validate from './validate';
import spinner from '../../img/Spinner200px.gif';

class InviteModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            requestInProgress: false,
            FullName: '',
            Email: '',
            ConfirmEmail: '',
            error:{}
        };
    }

    componentWillReceiveProps = (nextProps) => {
        const {show} = nextProps;
        if(!show){
            console.log("reset state");
            //reset state;
            this.setState({
                requestInProgress: false,
                FullName: '',
                Email: '',
                ConfirmEmail: '',
                error:{}
            });
        }
    }
    
    handleOnSubmit = async () => {
        //validate
        let errors = validate(this.state, true);
        this.setState({error: errors});

        //network request;
        if(Object.keys(errors).length === 0 && errors.constructor === Object){
            const { FullName, Email} = this.state;
            if(this.props.registerService) {
              await  this.props.registerService(FullName, Email);
            }
        }
    }
   
    validateInput = () => {
        let errors = validate(this.state, false);
        this.setState({error: errors});
    }

    handleOnChange = (e) => {
        let field = e.target.name;
        let value = e.target.value;
        this.setState({ [field]: value });
    }


    render() {
        const { show, isRequestInProgress, registrationError } = this.props;
        const buttonText = isRequestInProgress ? <span className="spinner"><img id="spinner" src={spinner} height="40" width="40" alt="spinner" /></span> : 'Send';

        let requestStatusText = '';
        if(registrationError){
            requestStatusText = <span className="text-danger">{registrationError}</span>
        } else if(isRequestInProgress){
            requestStatusText = 'Sending... Please wait.';
        }

        return (
            <Modal isOpen={show} centered toggle={this.toggle} className={this.props.className} size="md">
                <ModalHeader className="modal-header">Request an invite</ModalHeader>
                <ModalBody>
                    <Form>
                        <RenderField label="Full Name"
                            onChange={this.handleOnChange}
                            onBlur={this.validateInput}
                            invalid={this.state.isValidFullName}
                            error = {this.state.error.FullName}
                        />
                       <RenderField label="Email"
                            onChange={this.handleOnChange}
                            onBlur={this.validateInput}
                            invalid={this.state.isValidEmail}
                            error = {this.state.error.Email}
                        />
                       <RenderField label="Confirm Email"
                            onChange={this.handleOnChange}
                            onBlur={this.validateInput}
                            invalid={this.state.isValidEmail}
                            error = {this.state.error.ConfirmEmail}
                        />
                    </Form>
                </ModalBody>
                <ModalFooter className="d-flex flex-column">
                    <Button color="primary" size="lg" block id="sendrequest"
                        onClick={this.handleOnSubmit} 
                        disabled={isRequestInProgress}>{buttonText}
                    </Button>
                    <Button color="link" onClick={this.props.onCancel} id="cancelrequest">cancel</Button>
                    <p className="pt-1 h6 font-italic" id="requestStatusText">{requestStatusText}</p>
                </ModalFooter>
            </Modal>
        );
    }
}

InviteModal.propTypes = {
    show: PropTypes.bool.isRequired,
    isRequestInProgress: PropTypes.bool.isRequired,
    registerService: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    registrationError: PropTypes.string,
    
};

export default InviteModal;

const RenderField = ({ input, label, ...props }) => {
    const { error } = {...props};
    let isInvalid = !!(error);
    let id = label.replace(' ', '');
    return (
        <FormGroup row className="px-4">
            <Input placeholder={label} name={id} id={id} {...props} invalid={isInvalid}/>
            <FormFeedback id={`${id}-Error`}>{error}</FormFeedback>
        </FormGroup>
    );
}