import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return (
            <div className="footer">
                <p>Made with &#9829; in Auckland</p>
                <p>&#9400; 2018 Broccoli &amp; Co. All rights reserved.</p>
            </div>
        );
    }
}

export default Footer;