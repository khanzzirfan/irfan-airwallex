//import 'jsdom-global/register';
import React from 'react';
import RegisteredModal from './RegisteredModal';
import { shallow, mount, render } from 'enzyme';

describe("Registered Modal on default state", () => {

    let componentWrapper = null;
    let stubProps = {
        show: false,
        cancel: jest.fn(),
    };

    beforeEach(() => {
        componentWrapper = shallow(<RegisteredModal {...stubProps}/>);
    });

    it("component should exists and maps snapshot", ()=>{
        expect(componentWrapper).toBeDefined();
        expect(componentWrapper).toMatchSnapshot();
    });

    it("should render the component with given props", ()=>{
        expect(componentWrapper.instance().props.show).toEqual(false);
    });

});


describe("Registered Modal on show modal state", () => {

    let componentWrapper = null;
    let stubProps = {
        show: true,
        cancel: jest.fn(),
    };

    beforeEach(() => {
        componentWrapper = shallow(<RegisteredModal {...stubProps}/>);
    });

    it("component should exists and maps snapshot", ()=>{
        expect(componentWrapper).toBeDefined();
        expect(componentWrapper).toMatchSnapshot();
    });

    it("should render the component with given props", ()=>{
        expect(componentWrapper.instance().props.show).toEqual(true);
    });

    it("should render text components", ()=>{
        let wrapper = mount(<RegisteredModal {...stubProps}/>);
        let doneText = wrapper.find({id:'text1'});
        expect(doneText.length).toEqual(1);
        expect(doneText.text()).toEqual("All done!");

        let messageElement = wrapper.find({id:'text2'});
        expect(messageElement.length).toEqual(1);
        expect(messageElement.text()).toEqual("You will be one of the first to experience Broccoli & Co. when we open.");

        let okButtonElement = wrapper.find({id:'okButton'});
        expect(okButtonElement).toBeDefined();
    });

    it("should render text components", ()=>{
        let okClick = jest.fn();
        let wrapper = mount(<RegisteredModal {...stubProps} cancel={okClick}/>);
        wrapper.find({id:'okButton'}).first().simulate('click');
        expect(okClick).toHaveBeenCalled();
    });

});