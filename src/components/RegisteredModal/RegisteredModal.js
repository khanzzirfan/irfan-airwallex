import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Modal, ModalBody, ModalFooter} from 'reactstrap';

class RegisteredModal extends Component {
    render() {
        const {show} = this.props;
        return (
            <Modal isOpen={show} centered toggle={this.toggle} className={this.props.className} size="md">
                <ModalBody className="d-flex flex-column">
                    <p className="text-center text-success" id="text1"><u>All done!</u></p>
                    <p className="text-center" id="text2">You will be one of the first to experience Broccoli &amp; Co. when we open.</p>
                </ModalBody>
                <ModalFooter className="d-flex flex-column">
                    <Button color="success" size="lg" block  id="okButton"  onClick={this.props.cancel}>OK</Button>
                </ModalFooter>
            </Modal>
        );
    }
}

RegisteredModal.propTypes = {
    show: PropTypes.bool.isRequired,
    cancel: PropTypes.func
};

export default RegisteredModal;