import React, { Component } from 'react';
import './App.css';
import { Header, Footer, InviteModal, RegisteredModal } from './components';
import { submitRequest } from './service';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inviteModal: false,
      registered: false,
      registrationError: null,
      requestInProgress: false,
    }

  }

  onRegisterService = async (name, email) => {
    this.setState({ requestInProgress: true });
    try {
      const response = await submitRequest(name, email);
      this.setState({ inviteModal: false, registered: true, registrationError: null })
    }
    catch(ex){
      this.setState({ registered: false, registrationError: ex });
    }
    this.setState({ requestInProgress: false });
  }

  onRegisteredToggle = () => {
    this.setState({registered: false});
  }

  onCancelRequest = () =>{
    this.setState({ inviteModal: false });
  }
  onRequestInvite = () => {
    this.setState({ inviteModal: true });
  }

  render() {
    return (
      <div className="App Site">
        <div className="Site-content">
          <Header />
          <div className="main">
            <div>
              A better way to enjoy every day.
            </div>
            <p> Be first to know when we launch. </p>
            <button type="button" className="btn btn-light" id="requestinvite" onClick={this.onRequestInvite}>Request an invite </button>
            <InviteModal show={this.state.inviteModal}
              isRequestInProgress={this.state.requestInProgress}
              registerService={this.onRegisterService}
              registrationError={this.state.registrationError}
              onCancel = {this.onCancelRequest}
            />
          </div>
            <RegisteredModal show = {this.state.registered} cancel={this.onRegisteredToggle}/>
        </div >
        <Footer />
      </div>
    );
  }
}

export default App;
