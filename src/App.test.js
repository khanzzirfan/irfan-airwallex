import React from 'react';
import App from './App';
import { Header, Footer, InviteModal, RegisteredModal } from './components';
import * as API from './service';

import { shallow, mount, render } from 'enzyme';

describe("App default state", () => {

    let componentWrapper = null;
    let stubProps = {
        show: false,
        cancel: jest.fn(),
    };

    beforeEach(() => {
        componentWrapper = shallow(<App />);
    });

    it("component should exists and maps snapshot", () => {
        expect(componentWrapper).toBeDefined();
        expect(componentWrapper).toMatchSnapshot();
    });

    it("should render header and footer and request button", () => {
        let headerElement = componentWrapper.find('Header');
        expect(headerElement.length).toEqual(1)

        let footerElement = componentWrapper.find('Footer');
        expect(footerElement.length).toEqual(1)

        let buttonElement = componentWrapper.find({ id: 'requestinvite' });
        expect(buttonElement.length).toEqual(1);
    });

});

describe("App on modified state ", () => {

    let componentWrapper = null;
    var originalTimeout;
    const message = 'message';
    const mockResponse = (status, statusText, response) => {
        return new Response(response, {
            status: status,
            statusText: statusText,
            headers: {
                'Content-type': 'application/json'
            }
        });
    };

    const fakePromise = Promise.resolve(mockResponse(
        200,
        null,
        JSON.stringify({ message: message })
    ));

    beforeEach(function () {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 100;

        API.submitRequest = jest.fn().mockImplementationOnce(() => {
            return fakePromise
        });
    });

    afterEach(function () {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
    });

    it("should render Invite modal on request invite clicked", () => {
        let wrapper = mount(<App />);
        wrapper.find({ id: 'requestinvite' }).first().simulate('click');

        //assert invite modal is shown.
        expect(wrapper.state().inviteModal).toBe(true);
        let inviteModalElement = wrapper.find('InviteModal');
        expect(inviteModalElement.length).toBe(1);

        expect(wrapper.state().inviteModal).toBe(true);
        expect(wrapper.state().registered).toBe(false);
        expect(wrapper.state().registrationError).toEqual(null);
        expect(wrapper.state().requestInProgress).toBe(false);
    });


    it("should update the state when onRegisterService is called", () => {
        const wrapper = shallow(<App />);
        let instance = wrapper.instance();

        instance.onRegisterService('foo', 'ik@gmail.com');
        expect(wrapper.state().requestInProgress).toBe(true);

    });

    it("should update the state when onRegisteredToggle is called", () => {
        const wrapper = shallow(<App />);
        let instance = wrapper.instance();
        wrapper.setState({registered: true});
        instance.onRegisteredToggle();
        
        expect(wrapper.state()).toEqual({
            inviteModal: false,
            registered: false,
            registrationError: null,
            requestInProgress: false,
        });

    });

    it("should update the state when onCancelRequest is called", () => {
        const wrapper = shallow(<App />);
        let instance = wrapper.instance();
        wrapper.setState({inviteModal: true});

        instance.onCancelRequest();
        expect(wrapper.state()).toEqual({
            inviteModal: false,
            registered: false,
            registrationError: null,
            requestInProgress: false,
        });
    });

    it("should update the state when onRequestInvite is called", () => {
        const wrapper = shallow(<App />);
        let instance = wrapper.instance();
        wrapper.setState({inviteModal: false});

        instance.onRequestInvite();
        expect(wrapper.state()).toEqual({
            inviteModal: true,
            registered: false,
            registrationError: null,
            requestInProgress: false,
        });
    });


});
