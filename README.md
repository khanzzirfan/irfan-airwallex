# irfan-airwallex

Hello Reviewer
====

### To run this application follow steps on the root folder
- run npm install or yarn install
- yarn start
- should open up browser with localhost:3000. 

### Below are the features implemented in this application
- On request invite a modal popup has shown
- on send request a field validation is being done.
- should show field error if one of the field is empty or doesn't match email rules.
- on Successful request invite modal popu is closed and a registered modal is shown.

## Tests
With less time I have done about 70% of all the component tests and service tests. 
- Code coverage is been updated.
